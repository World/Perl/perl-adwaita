Adwaita
=======

Perl bindings to the 1.x series of the [libadwaita][adw] platform library for
writing GNOME applications.

This module allows you to GUI applications for the GNOME desktop environment in
a Perlish and object-oriented way, freeing you from the casting and memory
management in C, yet remaining very close in spirit to original API.

[adw]: https://gnome.pages.gitlab.gnome.org/libadwaita/

INSTALLATION
------------

To install this module from Git, you will need [dzil][dzil] installed

```
   dzil build
   dzil install
```

[dzil]: https://metacpan.org/pod/Dist::Zilla

DEPENDENCIES
------------

Adwaita needs the following C library, as well as its dependencies:

  - libadwaita ≥ 1.0

and these Perl modules:

  - Glib::Object::Introspection
  - Gtk4

COPYRIGHT AND LICENSE
---------------------

Copyright 2022 Emmanuele Bassi

This library is free software; you can redistribute it and/or modify it under
the terms of the GNU Library General Public License as published by the Free
Software Foundation; either version 2.1 of the License, or (at your option) any
later version.

This library is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.
